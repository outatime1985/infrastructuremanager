About IM:

Infrastructure Manager is an extension to the core solutions of the Automic system that provides an automated and repeatable way for you to create a new environment in any infrastructure provider, populate the environment with the correct middleware, and install a version of an application. The new application is then fully functional in your newly created environment.

The provisioning process combines the capabilities of Terraform, an open source infrastructure provisioning tool, with middleware provisioning tools (like Chef, Puppet, Ansible, and so on), and the Automic system.

You can use the Infrastructure Manager to provision any resources that are exposed by Terraform providers.

Environment provisioning happens throughout the application lifecycle; some environments might be created even before any application code is written, some during intensive development cycles, testing cycles, user acceptance and functional testing, and so on. In some cases, new production environments might be created, and later new environments for patch and bug fixing might be needed. Therefore, CA Automic offers a standalone module for provisioning environments that can be integrated with multiple tools and third-party tools to effectively and seamlessly provide full-stack environment provisioning at any stage of the CI/CD process.

The CDA integration with the Infrastructure Manager enables you to perform single-click full-stack provisioning for your environments, including the execution of CDA Workflows to deploy or test the Application. The implementation also provides access control and traceability on top of Terraform, along with full lifecycle management for the infrastructure.


